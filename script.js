
const tabs = document.getElementsByClassName("tabs-title");

const tabsContent = document.getElementsByClassName("tabs-text");

[...tabs].forEach(tab => tab.addEventListener('click', tabClick));

function tabClick(event) {
	const tabId = event.target.dataset.id;

	[...tabs].forEach((tab, i) => {
		tab.classList.remove('active');
		tabsContent[i].classList.remove('active');
	})

	tabs[tabId - 1].classList.add('active');
	tabsContent[tabId - 1].classList.add('active');
}
